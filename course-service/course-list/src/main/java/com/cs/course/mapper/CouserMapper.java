package com.cs.course.mapper;

import com.cs.course.domain.Course;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * FileName: CouserMapper
 * Date: 2022/2/9 15:30
 * Author:cs
 * Description: 课程的mapper类
 */
@Mapper
@Repository
public interface CouserMapper {
    @Select("select * from course where valid = 1")
    List<Course> findValidCourses();
}
