package com.cs.course.service.impl;

import com.cs.course.mapper.CouserMapper;
import com.cs.course.domain.Course;
import com.cs.course.service.CouserListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * FileName: CouserListServiceImpl
 * Date: 2022/2/9 15:28
 * Author:cs
 * Description:
 */
@Component
public class CouserListServiceImpl implements CouserListService {

    @Autowired
    CouserMapper couserMapper;

    @Override
    public List<Course> getCourseList() {
        return couserMapper.findValidCourses();
    }
}
