package com.cs.course;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * FileName: CourseApplication
 * Date: 2022/2/9 15:01
 * Author:cs
 * Description:项目启动类
 */
@Slf4j
@MapperScan("com.cs.course.mapper")
@SpringBootApplication
public class CourseApplication {
    public static void main(String[] args) {
        SpringApplication.run(CourseApplication.class, args);
        log.info("启动成功！！");
        log.info("地址: \thttp://127.0.0.1:8001");
    }
}
