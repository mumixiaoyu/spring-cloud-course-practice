package com.cs.course.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * FileName: Course
 * Date: 2022/2/9 15:18
 * Author:cs
 * Description:
 */
@Data
public class Course implements Serializable {

    private static final long serialVersionUID = 6852123876261624668L;
    Integer id;
    Integer courseId;
    String courseName;
    Integer valid;

}
