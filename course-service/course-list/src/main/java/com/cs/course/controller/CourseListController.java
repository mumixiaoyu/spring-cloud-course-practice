package com.cs.course.controller;

import com.cs.course.domain.Course;
import com.cs.course.service.CouserListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * FileName: CourseListController
 * Date: 2022/2/9 15:17
 * Author:cs
 * Description:课程列表
 */
@RestController
public class CourseListController {

    @Autowired
    private CouserListService couserListService;


    @GetMapping("/courses")
    public List<Course> courseList(){
        return couserListService.getCourseList();
    }
}
