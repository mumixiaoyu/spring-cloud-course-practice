package com.cs.course.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * FileName: Course
 * Date: 2022/2/9 15:18
 * Author:cs
 * Description:
 */
@Data
public class CoursePrice implements Serializable {


    private static final long serialVersionUID = -6003614363806777927L;
    Integer id;
    Integer courseId;
    Integer price;

}
