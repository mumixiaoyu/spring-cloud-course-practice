package com.cs.course.service.impl;

import com.cs.course.domain.CoursePrice;
import com.cs.course.mapper.CouserMapper;
import com.cs.course.service.CouserPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * FileName: CouserListServiceImpl
 * Date: 2022/2/9 15:28
 * Author:cs
 * Description:
 */
@Component
public class CouserPriceServiceImpl implements CouserPriceService {

    @Autowired
    CouserMapper couserMapper;

    @Override
    public CoursePrice getCourseList(Integer id) {
        return couserMapper.findCoursePrice(id);
    }
}
