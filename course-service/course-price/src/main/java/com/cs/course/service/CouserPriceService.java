package com.cs.course.service;

import com.cs.course.domain.CoursePrice;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * FileName: CouserListService
 * Date: 2022/2/9 15:27
 * Author:cs
 * Description: 课程列表服务
 */
@Service
public interface CouserPriceService {

    CoursePrice getCourseList(Integer id);
}
