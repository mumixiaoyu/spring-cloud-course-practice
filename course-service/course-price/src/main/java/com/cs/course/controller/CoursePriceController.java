package com.cs.course.controller;

import com.cs.course.service.CouserPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * FileName: CourseListController
 * Date: 2022/2/9 15:17
 * Author:cs
 * Description:课程价格控制器
 */
@RestController
public class CoursePriceController {

    @Autowired
    private CouserPriceService couserPriceService;


    @GetMapping("/price")
    public Integer courseList(Integer courseId) {
        return couserPriceService.getCourseList(courseId).getPrice();

    }
}
